# Nextcloud with docker

![https://cdn.icon-icons.com/icons2/2108/PNG/512/nextcloud_icon_130873.png](https://cdn.icon-icons.com/icons2/2108/PNG/512/nextcloud_icon_130873.png)
[Nextcloud](https://nextcloud.com/fr/) 
est un logiciel open-source permettant de reprendre la main sur nos données. 
Il intègre les quatre produits clés en main : Files, Talk, Groupware et Office en une seule plateforme, optimisant ainsi les flux du travail collaboratif.

Il existe plusieurs manières de déployer un nextcloud sur une machine. 

Votre objectif étant le cadre suivant : 

- Préparer un fichier docker-compose permettant de déployer un nextcloud hub. 
- Automatiser un script permettant d'automatiser les sauvegardes des données
- Automatiser un script permettant d'automatiser les sauvegardes de la base de données
